package com.nurrahman.latihan.presentation.components

import android.app.Application
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.nurrahman.latihan.presentation.atoms.SignalButton
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.ui.platform.LocalContext
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import com.nurrahman.latihan.framework.MovieViewModel

@Composable
fun OnBoardingCard(navController: NavController) {
    var context = LocalContext.current
    var viewModel = MovieViewModel(context)
    Column(modifier = Modifier.fillMaxWidth()) {
        Text(
            text = "Watch movie\r\nanywhere. anytime",
            style = TextStyle(
                fontSize = 32.sp,
                color = Color(0xFFFFFFFF),
                textAlign = TextAlign.Center
            ),
            modifier = Modifier.fillMaxWidth()
        )
    }

    Spacer(modifier = Modifier.height(24.dp))

    Text(
        text = "Feel the greatness of high resolution movies everywhere",
        style = TextStyle(
            fontSize = 20.sp,
            color = Color(0xFF7B9399),
            textAlign = TextAlign.Center
        ),
        modifier = Modifier.fillMaxWidth()
    )

    Spacer(modifier = Modifier.height(24.dp))

    SignalButton(buttonText = "Get Started", onClick = {
        viewModel.saveOnBoarding()
        navController.navigate("homeRoute")
    })
}