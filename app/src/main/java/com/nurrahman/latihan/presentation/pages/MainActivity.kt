package com.nurrahman.latihan.presentation.pages

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.nurrahman.latihan.framework.MovieViewModel
import com.nurrahman.latihan.ui.theme.LatihanTheme

class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            LatihanTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
//                    Greeting("Android")
                    Navigation()
                }
            }
        }
    }
}

@Composable
fun Navigation()
{
    val navigation = rememberNavController()
    NavHost(navigation, startDestination = "onboardRoute") {
        composable(route = "onboardRoute") {
            OnBoardingScreen(navigation)
        }
        composable(route = "homeRoute") {
            HomeScreen(navigation)
        }
    }
}

@Composable
fun Greeting(name: String, modifier: Modifier = Modifier) {
    Text(
        text = "Hello $name!",
        modifier = modifier
    )
}

@Preview(showBackground = true)
@Composable
fun GreetingPreview() {
    LatihanTheme {
        Greeting("Android")
    }
}