package com.nurrahman.latihan.presentation.pages

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.nurrahman.latihan.presentation.components.OnBoardingBanner
import com.nurrahman.latihan.presentation.components.OnBoardingCard


@Composable
fun OnBoardingScreen(navController: NavController) {

    Column(
        modifier = Modifier
            .background(Color.Black)
            .padding(8.dp)
            .fillMaxHeight(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Row(
            modifier = Modifier
                .weight(3f)
                .fillMaxHeight()
        ) {
            OnBoardingBanner(navController = navController)
        }

        Row(
            modifier = Modifier
                .padding(16.dp)
                .weight(2f)
                .fillMaxHeight()
        ) {
            Column {
                OnBoardingCard(navController = navController)
            }
        }
    }
}