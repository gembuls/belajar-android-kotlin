package com.nurrahman.latihan.presentation.atoms

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.*

@Composable
fun SignalButton(buttonText: String, onClick: () -> Unit = {}, modifier: Modifier = Modifier) {
    val gradient = Brush.linearGradient(
        listOf(Color(0xffD702FB), Color(0xff1A54FF)),
        start = Offset(0f,Float.POSITIVE_INFINITY),
        end = Offset(Float.POSITIVE_INFINITY,0f)
    )
    val modifier = Modifier.fillMaxWidth().padding(0.dp)
    Button(onClick = onClick,
        modifier = modifier,
        colors = ButtonDefaults.buttonColors(
            containerColor = Color.Transparent
        ),
    ) {
        Box(
            modifier = Modifier
                .background(gradient, shape = RoundedCornerShape(15.dp))
                .then(modifier)
                .height(50.dp),
            contentAlignment = Alignment.Center,
        ) {
            Text(
                text = buttonText,
                style = TextStyle(
                    fontSize = 20.sp,
                    fontWeight = FontWeight(400)
                )
            )
        }
    }
}