package com.nurrahman.latihan.presentation.pages

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.nurrahman.core.data.OnBoardingData
import com.nurrahman.latihan.framework.MovieViewModel
import com.nurrahman.latihan.presentation.atoms.SignalButton

@Composable
fun HomeScreen(navController: NavController) {
    var context = LocalContext.current
    var viewModel = MovieViewModel(context)

    Column(
        Modifier.padding(20.dp)
    ) {
        Button(onClick = {
            navController.navigate("onboardRoute")
        }) {
            Text(text = "Goto On Board Screen")
        }

    }
}