package com.nurrahman.latihan.framework

import android.app.Application
import android.content.Context
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.nurrahman.core.data.OnBoardingData
import com.nurrahman.core.repository.OnBoardingRepository
import com.nurrahman.core.usecase.GetOnBoarding
import com.nurrahman.core.usecase.SetOnBoarding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MovieViewModel(application: Context):ViewModel()
{
    private val coroutineScope = CoroutineScope(Dispatchers.IO)
    val repository = OnBoardingRepository(MovieDataSource(application))
    val useCases = UseCases(
        GetOnBoarding(repository),
        SetOnBoarding(repository)
    )

    val saved = MutableLiveData<Boolean>()

    fun saveOnBoarding()
    {
        var onBoardingData = OnBoardingData(isAlreadyOnBoarding = true)
        coroutineScope.launch {
            useCases.setOnBoarding(onBoardingData)
            saved.postValue(true)
        }
    }

    fun getOnBoarding()
    {
        coroutineScope.launch {
            var onBoardingData = useCases.getOnBoarding()
            print(onBoardingData)
        }

    }
}