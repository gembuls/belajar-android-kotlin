package com.nurrahman.latihan.framework.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.nurrahman.core.data.OnBoardingData

@Entity(tableName = "setup_table")
data class SetupEntity(
    @PrimaryKey(autoGenerate = true) val id : Int,
    @ColumnInfo(name = "setup_type") var setupType : String,
    @ColumnInfo(name = "setup_value") val setupValue : String,
){
//    companion object{
//        fun fromOnBoarding(onBoardingData: OnBoardingData){
//            SetupEntity(onBoardingData.isAlreadyOnBoarding, "on_boarding")
//        }
//    }
//
//    fun toOnBoarding(){
//        OnBoardingData(isAlreadyOnBoarding)
//    }
}
