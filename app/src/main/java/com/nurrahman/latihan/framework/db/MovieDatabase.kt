package com.nurrahman.latihan.framework.db

import android.content.Context
import android.graphics.Movie
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [SetupEntity::class], version = 1)
abstract class MovieDatabase: RoomDatabase() {

    companion object {
        private const val DATABASE_NAME = "movie_db"
        private var instance: MovieDatabase? = null

        private fun create(context: Context): MovieDatabase
        {
            return Room.databaseBuilder(context, MovieDatabase::class.java, DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .build()
        }

        fun getInstance(context: Context): MovieDatabase
        {
            return (instance ?: create(context)).also { instance = it }
        }
    }

    abstract fun setupDao() : SetupDao
}