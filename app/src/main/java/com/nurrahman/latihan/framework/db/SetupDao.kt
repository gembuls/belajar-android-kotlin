package com.nurrahman.latihan.framework.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query


@Dao
interface SetupDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun save(setupEntity: SetupEntity)

    @Query( "select * from setup_table where setup_type = :setupType")
    suspend fun findByType(setupType: String): SetupEntity
}