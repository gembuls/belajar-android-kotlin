package com.nurrahman.latihan.framework

import android.content.Context
import com.nurrahman.core.data.OnBoardingData
import com.nurrahman.core.repository.OnBoardingSource
import com.nurrahman.latihan.framework.db.MovieDatabase
import com.nurrahman.latihan.framework.db.SetupEntity

class MovieDataSource(context: Context): OnBoardingSource {
    val movieDao = MovieDatabase.getInstance(context).setupDao()

    override suspend fun setOnBoarding(onBoardingData: OnBoardingData) {
        var data = SetupEntity(id=1, setupType="ON_BOARDING", setupValue = onBoardingData.isAlreadyOnBoarding.toString())

        movieDao.save(data)
    }

    override suspend fun getOnBoarding() {
        movieDao.findByType("ON_BOARDING")
    }

}