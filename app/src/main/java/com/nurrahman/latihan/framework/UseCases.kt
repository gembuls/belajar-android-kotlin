package com.nurrahman.latihan.framework

import com.nurrahman.core.usecase.GetOnBoarding
import com.nurrahman.core.usecase.SetOnBoarding

data class UseCases(
    val getOnBoarding: GetOnBoarding,
    val setOnBoarding: SetOnBoarding
)