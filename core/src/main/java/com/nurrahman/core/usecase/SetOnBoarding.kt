package com.nurrahman.core.usecase

import com.nurrahman.core.data.OnBoardingData
import com.nurrahman.core.repository.OnBoardingRepository

class SetOnBoarding(private val onBoardingRepository: OnBoardingRepository) {
    suspend operator fun invoke(onBoardingData: OnBoardingData)
    {
        onBoardingRepository.setOnBoarding(onBoardingData)
    }
}