package com.nurrahman.core.usecase

import com.nurrahman.core.repository.OnBoardingRepository

class GetOnBoarding(private val onBoardingRepository: OnBoardingRepository) {
    suspend operator fun invoke()
    {
        return onBoardingRepository.getOnBoarding()
    }
}