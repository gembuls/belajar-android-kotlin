package com.nurrahman.core.repository

import com.nurrahman.core.data.OnBoardingData

class OnBoardingRepository(private val dataSource: OnBoardingSource) {

    suspend fun setOnBoarding(onBoardingData: OnBoardingData) {
        return dataSource.setOnBoarding(onBoardingData)
    }

    suspend fun getOnBoarding() {
        return dataSource.getOnBoarding()
    }

}