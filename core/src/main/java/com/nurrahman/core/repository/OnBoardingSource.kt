package com.nurrahman.core.repository

import com.nurrahman.core.data.OnBoardingData

interface OnBoardingSource {
    suspend fun setOnBoarding(onBoardingData: OnBoardingData)

    suspend fun getOnBoarding()
}